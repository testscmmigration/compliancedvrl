package com.moneygram.dvc.bo;

import java.io.Serializable;
import java.util.Date;

/**
 * ConsumerBO is used to encapsulate the Consumer information.
 * <p>
 * It is used as both input to the Consumer Validation Rule Manager and as input
 * to the rules to validate consumer name, address, phones and personal IDs.
 * 
 */
public class ConsumerBO implements Serializable {
	
	private static final long serialVersionUID = 8515993110229944313L;

	private String validationID;
	
	private String firstName;

    private String lastName;

    private String secondLastName;

    private String middleName;

    private AddressBO address;
    
    private String cellPhoneNumber;

    private String homePhoneNumber;

    private Date birthDate;

    private PersonalIDBO personalID1;

	private PersonalIDBO personalID2;

    private PersonalIDBO personalID3;

    public ConsumerBO() {
    }

	public ConsumerBO(String validationID, 
			String firstName, 
			String lastName,
			String secondLastName, 
			String middleName, 
			String addressLine1,
			String addressLine2, 
			String addressLine3, 
			String city,
			String postalCode, 
			String stateProvince, 
			String cellPhoneNumber,
			String homePhoneNumber, 
			String isoCountryCode, 
			Date birthDate,
			PersonalIDBO personalID1, 
			PersonalIDBO personalID2,
			PersonalIDBO personalID3) {
		this.validationID = validationID;
		this.firstName = firstName;
		this.lastName = lastName;
		this.secondLastName = secondLastName;
		this.middleName = middleName;
		this.address = new AddressBO(addressLine1, addressLine2, addressLine3,
				city, stateProvince, postalCode, isoCountryCode);
		this.cellPhoneNumber = cellPhoneNumber;
		this.homePhoneNumber = homePhoneNumber;
		this.birthDate = birthDate;
		this.personalID1 = personalID1;
		this.personalID2 = personalID2;
		this.personalID3 = personalID3;
	}

	/**
	 * Gets the validationID value for this Consumer.
	 * 
	 * @return validationID
	 */
	public String getValidationID() {
		return validationID;
	}
	
	/**
	 * Sets the validationID value for this Consumer.
	 * 
	 * @param validationID
	 */
	public void setValidationID(String validationID) {
		this.validationID = validationID;
	}
	
	
    /**
     * Gets the firstName value for this Consumer.
     * 
     * @return firstName
     */
    public java.lang.String getFirstName() {
        return firstName;
    }


    /**
     * Sets the firstName value for this Consumer.
     * 
     * @param firstName
     */
    public void setFirstName(java.lang.String firstName) {
        this.firstName = firstName;
    }


    /**
     * Gets the lastName value for this Consumer.
     * 
     * @return lastName
     */
    public java.lang.String getLastName() {
        return lastName;
    }


    /**
     * Sets the lastName value for this Consumer.
     * 
     * @param lastName
     */
    public void setLastName(java.lang.String lastName) {
        this.lastName = lastName;
    }


    /**
     * Gets the secondLastName value for this Consumer.
     * 
     * @return secondLastName
     */
    public java.lang.String getSecondLastName() {
        return secondLastName;
    }


    /**
     * Sets the secondLastName value for this Consumer.
     * 
     * @param secondLastName
     */
    public void setSecondLastName(java.lang.String secondLastName) {
        this.secondLastName = secondLastName;
    }


    /**
     * Gets the middleName value for this Consumer.
     * 
     * @return middleName
     */
    public java.lang.String getMiddleName() {
        return middleName;
    }


    /**
     * Sets the middleName value for this Consumer.
     * 
     * @param middleName
     */
    public void setMiddleName(java.lang.String middleName) {
        this.middleName = middleName;
    }

    /**
     * Gets the cellPhoneNumber value for this Consumer.
     * 
     * @return cellPhoneNumber
     */
    public java.lang.String getCellPhoneNumber() {
        return cellPhoneNumber;
    }


    /**
     * Sets the cellPhoneNumber value for this Consumer.
     * 
     * @param cellPhoneNumber
     */
    public void setCellPhoneNumber(java.lang.String cellPhoneNumber) {
        this.cellPhoneNumber = cellPhoneNumber;
    }


    /**
     * Gets the homePhoneNumber value for this Consumer.
     * 
     * @return homePhoneNumber
     */
    public java.lang.String getHomePhoneNumber() {
        return homePhoneNumber;
    }


    /**
     * Sets the homePhoneNumber value for this Consumer.
     * 
     * @param homePhoneNumber
     */
    public void setHomePhoneNumber(java.lang.String homePhoneNumber) {
        this.homePhoneNumber = homePhoneNumber;
    }

    /**
     * Gets the birthDate value for this Consumer.
     * 
     * @return birthDate
     */
    public java.util.Date getBirthDate() {
        return birthDate;
    }


    /**
     * Sets the birthDate value for this Consumer.
     * 
     * @param birthDate
     */
    public void setBirthDate(java.util.Date birthDate) {
        this.birthDate = birthDate;
    }


    /**
     * Gets the personalID1 value for this Consumer.
     * 
     * @return personalID1
     */
    public PersonalIDBO getPersonalID1() {
        return personalID1;
    }


    /**
     * Sets the personalID1 value for this Consumer.
     * 
     * @param personalID1
     */
    public void setPersonalID1(PersonalIDBO personalID1) {
        this.personalID1 = personalID1;
    }


    /**
     * Gets the personalID2 value for this Consumer.
     * 
     * @return personalID2
     */
    public PersonalIDBO getPersonalID2() {
        return personalID2;
    }


    /**
     * Sets the personalID2 value for this Consumer.
     * 
     * @param personalID2
     */
    public void setPersonalID2(PersonalIDBO personalID2) {
        this.personalID2 = personalID2;
    }


    /**
     * Gets the personalID3 value for this Consumer.
     * 
     * @return personalID3
     */
    public PersonalIDBO getPersonalID3() {
        return personalID3;
    }

    /**
     * Sets the personalID3 value for this Consumer.
     * 
     * @param personalID3
     */
    public void setPersonalID3(PersonalIDBO personalID3) {
        this.personalID3 = personalID3;
    }

    public AddressBO getAddress() {
		return address;
	}

	public void setAddress(AddressBO address) {
		this.address = address;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((birthDate == null) ? 0 : birthDate.hashCode());
		result = prime * result + ((cellPhoneNumber == null) ? 0 : cellPhoneNumber.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((homePhoneNumber == null) ? 0 : homePhoneNumber.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		result = prime * result + ((middleName == null) ? 0 : middleName.hashCode());
		result = prime * result + ((personalID1 == null) ? 0 : personalID1.hashCode());
		result = prime * result + ((personalID2 == null) ? 0 : personalID2.hashCode());
		result = prime * result + ((personalID3 == null) ? 0 : personalID3.hashCode());
		result = prime * result + ((secondLastName == null) ? 0 : secondLastName.hashCode());
		result = prime * result + ((validationID == null) ? 0 : validationID.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ConsumerBO other = (ConsumerBO) obj;
		if (address == null) {
			if (other.address != null) {
				return false;
			}
		} else if (!address.equals(other.address)) {
			return false;
		}
		if (birthDate == null) {
			if (other.birthDate != null) {
				return false;
			}
		} else if (!birthDate.equals(other.birthDate)) {
			return false;
		}
		if (cellPhoneNumber == null) {
			if (other.cellPhoneNumber != null) {
				return false;
			}
		} else if (!cellPhoneNumber.equals(other.cellPhoneNumber)) {
			return false;
		}
		if (firstName == null) {
			if (other.firstName != null) {
				return false;
			}
		} else if (!firstName.equals(other.firstName)) {
			return false;
		}
		if (homePhoneNumber == null) {
			if (other.homePhoneNumber != null) {
				return false;
			}
		} else if (!homePhoneNumber.equals(other.homePhoneNumber)) {
			return false;
		}
		if (lastName == null) {
			if (other.lastName != null) {
				return false;
			}
		} else if (!lastName.equals(other.lastName)) {
			return false;
		}
		if (middleName == null) {
			if (other.middleName != null) {
				return false;
			}
		} else if (!middleName.equals(other.middleName)) {
			return false;
		}
		if (personalID1 == null) {
			if (other.personalID1 != null) {
				return false;
			}
		} else if (!personalID1.equals(other.personalID1)) {
			return false;
		}
		if (personalID2 == null) {
			if (other.personalID2 != null) {
				return false;
			}
		} else if (!personalID2.equals(other.personalID2)) {
			return false;
		}
		if (personalID3 == null) {
			if (other.personalID3 != null) {
				return false;
			}
		} else if (!personalID3.equals(other.personalID3)) {
			return false;
		}
		if (secondLastName == null) {
			if (other.secondLastName != null) {
				return false;
			}
		} else if (!secondLastName.equals(other.secondLastName)) {
			return false;
		}
		if (validationID == null) {
			if (other.validationID != null) {
				return false;
			}
		} else if (!validationID.equals(other.validationID)) {
			return false;
		}
		return true;
	}

}
