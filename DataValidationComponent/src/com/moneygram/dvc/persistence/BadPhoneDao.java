package com.moneygram.dvc.persistence;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.moneygram.rule.common.datalist.RuleDataListFacade;

/**
 * BadPhoneDao provides the implementation of the {@link RuleDataListFacade}
 * to retrieve the bad phone number data list for a given business area.
 * <p>
 * The business area code should be set in the Spring configuration when this
 * DAO is configured and added to the Rule Data List Registry.
 * 
 */
public class BadPhoneDao extends AbstractBadDataListDao {

	protected static final String BAD_PHONE_PROC_NAME = "pkg_dvs_util.prc_phn_nbr_excp_cv";
	
	protected static final String BUSINESS_CODE  = "iv_bsns_area_code";
	protected static final String CALL_TYPE_CODE = "iv_call_type_code";
	protected static final String PROCESS_LOG_ID = "ov_prcs_log_id";
	protected static final String OUTPUT_CURSOR  = "ov_phn_nbr_excp_cv";
	
	public BadPhoneDao() {
		super();
	}

	@Override
	public RowMapper<?> getRowMapper() {
		return new BadPhoneMapper();
	}

	@Override
	public String getStoredProcedureName() {
		return BAD_PHONE_PROC_NAME;
	}

	@Override
	public String getProcessLogIdOutputName() {
		return PROCESS_LOG_ID;
	}

	@Override
	public String getBusinessAreaCodeInputName() {
		return BUSINESS_CODE;
	}

	@Override
	public String getCallTypeCodeInputName() {
		return CALL_TYPE_CODE;
	}

	@Override
	public String getCursorOutputName() {
		return OUTPUT_CURSOR;
	}

	/**
	 * BadPhoneMapper processes a {@link ResultSet} to populate a {@link String}
	 * object.
	 * 
	 */
	protected static final class BadPhoneMapper implements RowMapper<String> {

		private static final String PHONE_NUMBER_COLUMN = "phn_nbr";

		@Override
		public String mapRow(ResultSet resultset, int i) throws SQLException {
			return resultset.getString(PHONE_NUMBER_COLUMN);
		}
	}
}
