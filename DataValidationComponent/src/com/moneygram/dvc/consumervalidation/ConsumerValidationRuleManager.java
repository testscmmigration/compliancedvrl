package com.moneygram.dvc.consumervalidation;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

import com.moneygram.dvc.bo.AddressBO;
import com.moneygram.dvc.bo.ConsumerBO;
import com.moneygram.dvc.bo.ConsumerNameBO;
import com.moneygram.dvc.bo.RuleErrorBO;
import com.moneygram.rule.common.cache.RuleDataListFactory;
import com.moneygram.rule.common.standardrule.StandardRuleResult;
import com.moneygram.rule.common.standardrule.StandardRuleSetExecContext;
import com.moneygram.rule.common.standardrule.StandardRuleSetResult;
import com.moneygram.rule.framework.IRuleSetExecContext;
import com.moneygram.rule.framework.RuleSetManager;

/**
 * ConsumerValidationRuleManager manages the rule execution and integration with
 * the MGI rules framework.
 * 
 */
public class ConsumerValidationRuleManager implements InitializingBean {

	/**
	 * Validates the {@link List} of {@link ConsumerBO} objects.
	 * 
	 * @param consumers
	 *            {@link List} of {@link ConsumerBO} to validate.
	 * @return a {@link List} of {@link RuleErrorBO}. An empty list indicates
	 *         all consumers were valid.
	 * @throws Exception 
	 */
	public List<RuleErrorBO> validate(List<ConsumerBO> consumers) throws Exception {
		Assert.notNull(consumers, "Consumers cannot be null");
		Assert.notEmpty(consumers);
		
		List<RuleErrorBO> errorList = new ArrayList<RuleErrorBO>();
		
		ConsumerValidationRuleSetInput ruleSetInput = new ConsumerValidationRuleSetInput();
		ruleSetInput.setBadPhoneList((List<String>) ruleDataListFactory.getRuleDataList(badPhoneListKeyName));
		ruleSetInput.setBadAddressList((List<AddressBO>) ruleDataListFactory.getRuleDataList(badAddressListKeyName));
		ruleSetInput.setBadConsumerNameList((List<ConsumerNameBO>) ruleDataListFactory.getRuleDataList(badConsumerNameListKeyName));
		
		StandardRuleSetResult ruleSetResult = null;
		
		// Loop through each consumer.
		for (ConsumerBO consumer : consumers) {
			ruleSetInput.setConsumer(consumer);
			ruleSetResult = new StandardRuleSetResult();
			
			IRuleSetExecContext ruleExecContext = new StandardRuleSetExecContext(ruleSetInput, ruleSetResult);
			
			ruleSetManager.execute(ruleExecContext);
			
			if (ruleSetResult.getRuleResultList() == null || ruleSetResult.getRuleResultList().isEmpty()) {
				continue;
			}
			
			for (StandardRuleResult ruleResult : ruleSetResult.getRuleResultList()) {
				
				RuleErrorBO error = new RuleErrorBO(consumer.getValidationID(), ruleResult.getActionObject().getRuleError());
				
				errorList.add(error);
			}
		}
		
		return errorList;
	}
	
	@Override
	public void afterPropertiesSet() throws Exception {
		Assert.notNull(ruleSetManager, "ruleSetFactory is null.");
		Assert.notNull(ruleDataListFactory, "ruleDataListFactory is null.");
		Assert.hasText(badPhoneListKeyName, "badPhoneListKeyName is not set.");
		Assert.hasText(badAddressListKeyName, "badAddressListKeyName is not set.");
		Assert.hasText(badConsumerNameListKeyName, "badConsumerNameListKeyName is not set.");
	}

	public void setRuleSetManager(RuleSetManager ruleSetManager) {
		this.ruleSetManager = ruleSetManager;
	}
	
	public void setRuleDataListFactory(RuleDataListFactory ruleDataListFactory) {
		this.ruleDataListFactory = ruleDataListFactory;
	}
	
	public void setBadPhoneListKeyName(String badPhoneListKeyName) {
		this.badPhoneListKeyName = badPhoneListKeyName;
	}
	
	public void setBadAddressListKeyName(String badAddressListKeyName) {
		this.badAddressListKeyName = badAddressListKeyName;
	}
	
	public void setBadConsumerNameListKeyName(String badConsumerNameListKeyName) {
		this.badConsumerNameListKeyName = badConsumerNameListKeyName;
	}
	
	// Spring DI properties.
	private RuleSetManager ruleSetManager;
	private RuleDataListFactory ruleDataListFactory;
	private String badPhoneListKeyName;
	private String badAddressListKeyName;
	private String badConsumerNameListKeyName;
}
