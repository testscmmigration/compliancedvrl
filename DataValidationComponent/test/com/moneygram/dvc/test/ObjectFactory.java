package com.moneygram.dvc.test;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import com.moneygram.dvc.bo.AddressBO;
import com.moneygram.dvc.bo.ConsumerBO;
import com.moneygram.dvc.bo.PersonalIDBO;
import com.moneygram.rule.framework.bo.RuleConfig;

public class ObjectFactory {

	public static ConsumerBO createMinimalValidConsumer() {
		ConsumerBO consumer = new ConsumerBO();
		
		consumer.setValidationID(UUID.randomUUID().toString());
		consumer.setFirstName("Firstname");
		consumer.setLastName("Lastname");
		
		return consumer;
	}

	public static ConsumerBO createMinimalInvalidConsumer() {
		ConsumerBO consumer = new ConsumerBO();
		
		consumer.setValidationID(UUID.randomUUID().toString());
		consumer.setFirstName("First,name");
		consumer.setLastName("Last,name");
		
		return consumer;
	}

	public static List<RuleConfig> buildRuleConfigList(String systemCode, String categoryCode) throws IOException {
		List<RuleConfig> ruleConfigList = new ArrayList<RuleConfig>();
		ruleConfigList.add(buildRuleConfigStd(1, "RULE1", systemCode, categoryCode, 5, "rules/consumer_fname_rule1.xml", "com.moneygram.rule.common.standardrule.StandardRule"));
		ruleConfigList.add(buildRuleConfigStd(2, "RULE2", systemCode, categoryCode, 10, "rules/consumer_fname_rule2.xml", "com.moneygram.rule.common.standardrule.StandardRule"));
		ruleConfigList.add(buildRuleConfigStd(3, "RULE3", systemCode, categoryCode, 15, "rules/consumer_lname_rule1.xml", "com.moneygram.rule.common.standardrule.StandardRule"));
		ruleConfigList.add(buildRuleConfigStd(4, "RULE4", systemCode, categoryCode, 15, "rules/consumer_lname_rule2.xml", "com.moneygram.rule.common.standardrule.StandardRule"));
		ruleConfigList.add(buildRuleConfigStd(5, "RULE5", systemCode, categoryCode, 15, "rules/consumer_secondLastname_rule1.xml", "com.moneygram.rule.common.standardrule.StandardRule"));
		ruleConfigList.add(buildRuleConfigStd(6, "RULE6", systemCode, categoryCode, 15, "rules/consumer_secondLastname_rule2.xml", "com.moneygram.rule.common.standardrule.StandardRule"));
		ruleConfigList.add(buildRuleConfigStd(7, "RULE7", systemCode, categoryCode, 15, "rules/consumer_middlename_rule1.xml", "com.moneygram.rule.common.standardrule.StandardRule"));
		ruleConfigList.add(buildRuleConfigStd(8, "RULE8", systemCode, categoryCode, 15, "rules/consumer_middlename_rule2.xml", "com.moneygram.rule.common.standardrule.StandardRule"));
		ruleConfigList.add(buildRuleConfigStd(9, "RULE9", systemCode, categoryCode, 15, "rules/consumer_HomePhone_rule1.xml", "com.moneygram.rule.common.standardrule.StandardRule"));
		ruleConfigList.add(buildRuleConfigStd(10, "RULE10", systemCode, categoryCode, 15, "rules/consumer_HomePhone_rule2.xml", "com.moneygram.rule.common.standardrule.StandardRule"));
		ruleConfigList.add(buildRuleConfigStd(11, "RULE11", systemCode, categoryCode, 15, "rules/consumer_HomePhone_rule3.xml", "com.moneygram.rule.common.standardrule.StandardRule"));
		ruleConfigList.add(buildRuleConfigStd(12, "RULE12", systemCode, categoryCode, 15, "rules/consumer_HomePhone_rule4.xml", "com.moneygram.rule.common.standardrule.StandardRule"));
		ruleConfigList.add(buildRuleConfigStd(13, "RULE13", systemCode, categoryCode, 15, "rules/consumer_CellPhone_rule1.xml", "com.moneygram.rule.common.standardrule.StandardRule"));
		ruleConfigList.add(buildRuleConfigStd(14, "RULE14", systemCode, categoryCode, 15, "rules/consumer_CellPhone_rule2.xml", "com.moneygram.rule.common.standardrule.StandardRule"));
		ruleConfigList.add(buildRuleConfigStd(15, "RULE15", systemCode, categoryCode, 15, "rules/consumer_CellPhone_rule3.xml", "com.moneygram.rule.common.standardrule.StandardRule"));
		ruleConfigList.add(buildRuleConfigStd(16, "RULE16", systemCode, categoryCode, 15, "rules/consumer_CellPhone_rule4.xml", "com.moneygram.rule.common.standardrule.StandardRule"));
		ruleConfigList.add(buildRuleConfigStd(17, "RULE17", systemCode, categoryCode, 15, "rules/consumer_Address_rule1.xml", "com.moneygram.rule.common.standardrule.StandardRule"));
		ruleConfigList.add(buildRuleConfigStd(18, "RULE18", systemCode, categoryCode, 15, "rules/consumer_Address_rule2.xml", "com.moneygram.rule.common.standardrule.StandardRule"));
		ruleConfigList.add(buildRuleConfigStd(19, "RULE19", systemCode, categoryCode, 15, "rules/consumer_Address_rule3.xml", "com.moneygram.rule.common.standardrule.StandardRule"));
		ruleConfigList.add(buildRuleConfigStd(20, "RULE20", systemCode, categoryCode, 15, "rules/consumer_Address_rule4.xml", "com.moneygram.rule.common.standardrule.StandardRule"));
		ruleConfigList.add(buildRuleConfigStd(21, "RULE21", systemCode, categoryCode, 15, "rules/consumer_Address_rule5.xml", "com.moneygram.rule.common.standardrule.StandardRule"));
		ruleConfigList.add(buildRuleConfigStd(22, "RULE22", systemCode, categoryCode, 15, "rules/consumer_Address_Rule_City.xml", "com.moneygram.rule.common.standardrule.StandardRule"));
		ruleConfigList.add(buildRuleConfigStd(23, "RULE23", systemCode, categoryCode, 15, "rules/consumer_Address_Rule_State.xml", "com.moneygram.rule.common.standardrule.StandardRule"));
		ruleConfigList.add(buildRuleConfigStd(24, "RULE24", systemCode, categoryCode, 15, "rules/consumer_Address_Rule_Country.xml", "com.moneygram.rule.common.standardrule.StandardRule"));
		ruleConfigList.add(buildRuleConfigStd(25, "RULE25", systemCode, categoryCode, 15, "rules/Personal_ID1_rule1.xml", "com.moneygram.rule.common.standardrule.StandardRule"));
		ruleConfigList.add(buildRuleConfigStd(26, "RULE26", systemCode, categoryCode, 15, "rules/Personal_ID1_rule2.xml", "com.moneygram.rule.common.standardrule.StandardRule"));
		ruleConfigList.add(buildRuleConfigStd(27, "RULE27", systemCode, categoryCode, 15, "rules/Personal_ID1_rule3.xml", "com.moneygram.rule.common.standardrule.StandardRule"));
		ruleConfigList.add(buildRuleConfigStd(28, "RULE28", systemCode, categoryCode, 15, "rules/Personal_ID2_rule1.xml", "com.moneygram.rule.common.standardrule.StandardRule"));
		ruleConfigList.add(buildRuleConfigStd(29, "RULE29", systemCode, categoryCode, 15, "rules/Personal_ID2_rule2.xml", "com.moneygram.rule.common.standardrule.StandardRule"));
		ruleConfigList.add(buildRuleConfigStd(30, "RULE30", systemCode, categoryCode, 15, "rules/Personal_ID2_rule3.xml", "com.moneygram.rule.common.standardrule.StandardRule"));
		ruleConfigList.add(buildRuleConfigStd(31, "RULE31", systemCode, categoryCode, 15, "rules/Personal_ID3_rule1.xml", "com.moneygram.rule.common.standardrule.StandardRule"));
		ruleConfigList.add(buildRuleConfigStd(32, "RULE32", systemCode, categoryCode, 15, "rules/Personal_ID3_rule2.xml", "com.moneygram.rule.common.standardrule.StandardRule"));
		ruleConfigList.add(buildRuleConfigStd(33, "RULE33", systemCode, categoryCode, 15, "rules/Personal_ID3_rule3.xml", "com.moneygram.rule.common.standardrule.StandardRule"));
		ruleConfigList.add(buildRuleConfigStd(34, "RULE34", systemCode, categoryCode, 15, "rules/consumer_Address_PostalCode_Rule1.xml", "com.moneygram.rule.common.standardrule.StandardRule"));
		ruleConfigList.add(buildRuleConfigStd(35, "RULE35", systemCode, categoryCode, 15, "rules/consumer_Address_PostalCode_Rule2.xml", "com.moneygram.rule.common.standardrule.StandardRule"));
		ruleConfigList.add(buildRuleConfigStd(36, "RULE36", systemCode, categoryCode, 15, "rules/consumer_Address_PostalCode_Rule3.xml", "com.moneygram.rule.common.standardrule.StandardRule"));
		ruleConfigList.add(buildRuleConfigStd(37, "RULE37", systemCode, categoryCode, 15, "rules/consumer_Date_of_Birth_Rule1.xml", "com.moneygram.rule.common.standardrule.StandardRule"));
		ruleConfigList.add(buildRuleConfigStd(38, "RULE38", systemCode, categoryCode, 15, "rules/consumer_Date_of_Birth_Rule2.xml", "com.moneygram.rule.common.standardrule.StandardRule"));
		
		ruleConfigList.add(buildRuleConfigStd(40, "RULE40", systemCode, categoryCode, 15, "rules/Personal_Id1_rule4.xml", "com.moneygram.rule.common.standardrule.StandardRule"));
		ruleConfigList.add(buildRuleConfigStd(41, "RULE41", systemCode, categoryCode, 15, "rules/Personal_Id2_rule4.xml", "com.moneygram.rule.common.standardrule.StandardRule"));
		ruleConfigList.add(buildRuleConfigStd(42, "RULE42", systemCode, categoryCode, 15, "rules/Personal_Id3_rule4.xml", "com.moneygram.rule.common.standardrule.StandardRule"));
		return ruleConfigList;
	}
	
	public static RuleConfig buildRuleConfigStd(int ruleConfigId, String ruleCode, String sysCode, 
			String catCode, int execOrder, String fileName, String className) throws IOException{
		RuleReader reader = new RuleReader();
		RuleConfig ruleConfig = new RuleConfig();
		ruleConfig.setBegDate(Calendar.getInstance());
		Calendar now = Calendar.getInstance();
		now.add(Calendar.YEAR, 1);
		ruleConfig.setEndDate(now);
		ruleConfig.setExecOrderNbr(execOrder);
		ruleConfig.setRuleCatCode(catCode);
		ruleConfig.setRuleCode(ruleCode);
		ruleConfig.setRuleConfigId(ruleConfigId);
		ruleConfig.setRuleImplClass(className);
		ruleConfig.setRuleParam(reader.read(fileName));
		ruleConfig.setRuleSysCode(sysCode);
		
		return ruleConfig;
	}

	public static ConsumerBO createValidSecondLastNameConsumer() {
		ConsumerBO consumer = createMinimalValidConsumer();
		consumer.setSecondLastName("Secondlastname");
		return consumer;
	}

	public static ConsumerBO createInValidSecondLastNameConsumer() {
		ConsumerBO consumer = createMinimalValidConsumer();
		consumer.setSecondLastName("2nd,lastname");
		return consumer;
	}

	public static ConsumerBO createValidMiddleNameConsumer() {
		ConsumerBO consumer = createMinimalValidConsumer();
		consumer.setMiddleName("Middlename");
		return consumer;
	}

	public static ConsumerBO createInValidMiddleNameConsumer() {
		ConsumerBO consumer = createMinimalValidConsumer();
		consumer.setMiddleName("Middl3,name");
		return consumer;
	}

	public static ConsumerBO createValidHomePhoneConsumer() {
		ConsumerBO consumer = createMinimalValidConsumer();
		consumer.setHomePhoneNumber("5551234");
		return consumer;
	}

	public static ConsumerBO createInValidHomePhoneConsumer() {
		ConsumerBO consumer = createMinimalValidConsumer();
		consumer.setHomePhoneNumber("5a-A");
		return consumer;
	}

	public static ConsumerBO createBadPhoneListHomePhoneConsumer() {
		ConsumerBO consumer = createMinimalValidConsumer();
		consumer.setHomePhoneNumber("123456789");
		return consumer;
	}

	public static AddressBO createMinimalValidAddress() {
		AddressBO address = new AddressBO();
		address.setAddressLine1("1515 Utica Ave. S.");
		address.setAddressLine2("#100");
		address.setAddressLine3("c/o Dan");
		address.setCity("St. Louis Park");
		address.setStateProvince("MN");
		address.setIsoCountryCode("USA");
		address.setPostalCode("55416");
		return address;
	}

	public static AddressBO createBadAddressListAddress() {
		AddressBO address = new AddressBO();
		address.setAddressLine1("NCL SKY");
		address.setCity("MIAMI");
		address.setIsoCountryCode("USA");
		address.setPostalCode("99999");
		address.setStateProvince("FL");	
		return address;
	}

	public static PersonalIDBO createMinimalValidPersonalID() {
		PersonalIDBO personalID = new PersonalIDBO();
		personalID.setIsoCountryCode("USA");
		personalID.setStateProvince("MN");
		personalID.setPersonalIDNumber("T555123466741");
		
		return personalID;
	}
}
