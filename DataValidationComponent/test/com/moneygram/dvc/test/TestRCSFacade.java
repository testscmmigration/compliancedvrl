package com.moneygram.dvc.test;

import java.util.List;

import com.moneygram.rule.common.rcs.manager.RuleConfigurationFacade;
import com.moneygram.rule.framework.bo.RuleConfig;

public class TestRCSFacade implements RuleConfigurationFacade {

	@Override
	public List<RuleConfig> getRuleConfigurationsBySystemCategory(
			String systemCode, String categoryCode) throws Exception {
		
		return ObjectFactory.buildRuleConfigList(systemCode, categoryCode);
	}

}
