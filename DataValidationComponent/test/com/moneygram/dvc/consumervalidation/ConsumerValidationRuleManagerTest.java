package com.moneygram.dvc.consumervalidation;

import static org.junit.Assert.fail;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.moneygram.dvc.bo.AddressBO;
import com.moneygram.dvc.bo.ConsumerBO;
import com.moneygram.dvc.bo.PersonalIDBO;
import com.moneygram.dvc.bo.RuleErrorBO;
import com.moneygram.dvc.test.ObjectFactory;
import com.moneygram.dvc.test.TestRCSFacade;
import com.moneygram.rule.common.cache.CachedRuleSetFactory;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath*:/META-INF/spring/dvc-test-component.xml")
public class ConsumerValidationRuleManagerTest implements ApplicationContextAware {

	@Autowired
	ConsumerValidationRuleManager validator;
	
	@Before
	public void setUp() throws Exception {
		CachedRuleSetFactory crsf =	(CachedRuleSetFactory) appCtx.getBean("mgirulecommon.cachedRuleSetFactory");
		crsf.setRuleConfigurationFacade(new TestRCSFacade());
	}

	@Test(expected=IllegalArgumentException.class)
	public void testValidate_InvalidInput_Null() throws Exception {
		validator.validate(null);
		fail("Expected an IllegalArgumentException to be thrown.");
		
		List<ConsumerBO> consumers = Collections.emptyList();
		validator.validate(consumers);
		fail("Expected an IllegalArgumentException to be thrown.");
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void testValidate_InvalidInput_EmptyList() throws Exception {
		List<ConsumerBO> consumers = Collections.emptyList();
		validator.validate(consumers);
		fail("Expected an IllegalArgumentException to be thrown.");
	}
	
	@Test
	public void testValidate_ReturnNotNull() throws Exception {
		List<ConsumerBO> consumers = new ArrayList<ConsumerBO>();
		consumers.add(ObjectFactory.createMinimalValidConsumer());
		
		List<RuleErrorBO> results = validator.validate(consumers);
		Assert.assertNotNull("result is null.", results);
	}

	@Test
	public void testValidate_OneValidConsumer() throws Exception {
		List<ConsumerBO> consumers = new ArrayList<ConsumerBO>();
		ConsumerBO consumer = ObjectFactory.createMinimalValidConsumer();
		consumers.add(consumer);
		
		List<RuleErrorBO> errorList = validator.validate(consumers);
		Assert.assertTrue("errorList should be empty", errorList.isEmpty());
	}
	
	@Test 
	public void testValidate_MultipleValidConsumers() throws Exception {
		List<ConsumerBO> consumers = new ArrayList<ConsumerBO>();
		
		consumers.add(ObjectFactory.createMinimalValidConsumer());
		consumers.add(ObjectFactory.createMinimalValidConsumer());
		consumers.add(ObjectFactory.createMinimalValidConsumer());
		
		List<RuleErrorBO> errorList = validator.validate(consumers);
		Assert.assertTrue("errorList should be empty", errorList.isEmpty());
	}
	
	@Test
	public void testValidate_OneInvalidConsumer() throws Exception {
		List<ConsumerBO> consumers = new ArrayList<ConsumerBO>();
		ConsumerBO consumer = ObjectFactory.createMinimalInvalidConsumer();
		consumers.add(consumer);
		
		List<RuleErrorBO> errorList = validator.validate(consumers);
		Assert.assertFalse("errorList should have at least one item", errorList.isEmpty());
		
		for (RuleErrorBO ruleError : errorList) {
			Assert.assertEquals("Validation ID mis-match:", consumer.getValidationID(), ruleError.getValidationID());
		}
	}
	
	@Test
	public void testValidate_MultipleInvalidConsumers() throws Exception {
		List<ConsumerBO> consumers = new ArrayList<ConsumerBO>();
		Map<String, ConsumerBO> consumerMap = generateMinimalInvalidConsumerMap(); 
		ConsumerBO consumer;

		consumers.addAll(consumerMap.values());
		
		List<RuleErrorBO> errorList = validator.validate(consumers);
		Assert.assertFalse("errorList should have at least one item", errorList.isEmpty());
		
		Set<ConsumerBO> found = new HashSet<ConsumerBO>();
		
		for (RuleErrorBO ruleErrorBO : errorList) {
			consumer = consumerMap.get(ruleErrorBO.getValidationID());
			found.add(consumer);
		}

		Assert.assertTrue("Did not find all invalid consumers", found.containsAll(consumers));
	}

	@Test
	public void testValidate_MixOfValidAndInvalidConsumers() throws Exception {
		Map<String, ConsumerBO> validConsumers = generateMinimalValidConsumerMap();
		Map<String, ConsumerBO> invalidConsumers = generateMinimalInvalidConsumerMap();
		
		List<ConsumerBO> consumers = new ArrayList<ConsumerBO>();
		consumers.addAll(validConsumers.values());
		consumers.addAll(invalidConsumers.values());
		
		List<RuleErrorBO> errorList = validator.validate(consumers);
		Assert.assertFalse("errorList should have at least one item", errorList.isEmpty());
		
		Set<String> found = new HashSet<String>();
		
		for (RuleErrorBO ruleErrorBO : errorList) {
			found.add(ruleErrorBO.getValidationID());
			Assert.assertTrue("Contains a valid consumer validation ID", invalidConsumers.containsKey(ruleErrorBO.getValidationID()));
		}
		
		Assert.assertTrue("Did not find all invalid consumers", found.containsAll(invalidConsumers.keySet()));
	}
	
	@Test
	public void testSecondLastName_Valid() throws Exception {
		List<ConsumerBO> consumers = new ArrayList<ConsumerBO>();
		ConsumerBO consumer = ObjectFactory.createValidSecondLastNameConsumer();
		consumers.add(consumer);
		
		List<RuleErrorBO> errorList = validator.validate(consumers);
		Assert.assertTrue("errorList should be empty", errorList.isEmpty());
	}
	
	@Test 
	public void testSecondLastName_Invalid() throws Exception {
		Set<String> expectedErrors = new HashSet<String>();
		expectedErrors.add("368");
		expectedErrors.add("369");
		
		ConsumerBO consumer = ObjectFactory.createInValidSecondLastNameConsumer();
		
		runInvalid(consumer, expectedErrors);
	}

	@Test
	public void testMiddleName_Valid() throws Exception {
		List<ConsumerBO> consumers = new ArrayList<ConsumerBO>();
		ConsumerBO consumer = ObjectFactory.createValidMiddleNameConsumer();
		consumers.add(consumer);
		
		List<RuleErrorBO> errorList = validator.validate(consumers);
		Assert.assertTrue("errorList should be empty", errorList.isEmpty());
	}
	
	@Test
	public void testMiddleName_Invalid() throws Exception {
		ConsumerBO consumer = ObjectFactory.createInValidMiddleNameConsumer();
		
		Set<String> expectedErrors = new HashSet<String>();
		expectedErrors.add("368");
		expectedErrors.add("369");
		
		runInvalid(consumer, expectedErrors);
	}
	
	@Test
	public void testHomePhone_Valid() throws Exception {
		List<ConsumerBO> consumers = new ArrayList<ConsumerBO>();
		ConsumerBO consumer = ObjectFactory.createValidHomePhoneConsumer();
		consumers.add(consumer);
		
		List<RuleErrorBO> errorList = validator.validate(consumers);
		Assert.assertTrue("errorList should be empty", errorList.isEmpty());
		
		consumer.setHomePhoneNumber("9999999999");
		errorList = validator.validate(consumers);
		Assert.assertTrue("errorList should be empty", errorList.isEmpty());
	}

	@Test
	public void testHomePhone_Invalid() throws Exception {
		ConsumerBO consumer = ObjectFactory.createInValidHomePhoneConsumer();
		
		Set<String> expectedErrors = new HashSet<String>();
		expectedErrors.add("380");
		expectedErrors.add("381");
		
		runInvalid(consumer, expectedErrors);
		
		// Test all the same number.
		expectedErrors.clear();
		expectedErrors.add("667");
		consumer.setHomePhoneNumber("0000000000");
		runInvalid(consumer, expectedErrors);
		
		consumer.setHomePhoneNumber("1111111111");
		runInvalid(consumer, expectedErrors);

		consumer.setHomePhoneNumber("2222222222");
		runInvalid(consumer, expectedErrors);

		consumer.setHomePhoneNumber("3333333333");
		runInvalid(consumer, expectedErrors);

		consumer.setHomePhoneNumber("4444444444");
		runInvalid(consumer, expectedErrors);

		consumer.setHomePhoneNumber("5555555555");
		runInvalid(consumer, expectedErrors);

		consumer.setHomePhoneNumber("6666666666");
		runInvalid(consumer, expectedErrors);

		consumer.setHomePhoneNumber("7777777777");
		runInvalid(consumer, expectedErrors);

		consumer.setHomePhoneNumber("8888888888");
		runInvalid(consumer, expectedErrors);

		// Numeric only.
		expectedErrors.clear();
		expectedErrors.add("380");
		consumer.setHomePhoneNumber("AaBbCcDdEe");
		runInvalid(consumer, expectedErrors);
		
	}
	
	@Test
	public void testHomePhone_InBadPhoneList() throws Exception {
		ConsumerBO consumer = ObjectFactory.createBadPhoneListHomePhoneConsumer();
		
		Set<String> expectedErrors = new HashSet<String>();
		expectedErrors.add("383");
		
		runInvalid(consumer, expectedErrors);
	}
	
	@Test
	public void testCellPhone_Valid() throws Exception {
		List<ConsumerBO> consumers = new ArrayList<ConsumerBO>();
		ConsumerBO consumer = ObjectFactory.createMinimalValidConsumer();
		consumer.setCellPhoneNumber("9525913120");
		consumers.add(consumer);
		
		List<RuleErrorBO> errorList = validator.validate(consumers);
		Assert.assertTrue("errorList should be empty", errorList.isEmpty());
	}

	@Test
	public void testCellPhone_Invalid() throws Exception {
		ConsumerBO consumer = ObjectFactory.createMinimalValidConsumer();
		Set<String> expectedErrors = new HashSet<String>();
		
		// Too small.
		consumer.setCellPhoneNumber("123");
		expectedErrors.add("380");
		runInvalid(consumer, expectedErrors);
		
		// Alpha
		consumer.setCellPhoneNumber("12ABC67");
		expectedErrors.clear();
		expectedErrors.add("380");
		runInvalid(consumer, expectedErrors);
		
		// Special characters
		consumer.setCellPhoneNumber("555#666");
		expectedErrors.clear();
		expectedErrors.add("380");
		expectedErrors.add("381");
		runInvalid(consumer, expectedErrors);
		
		// In Bad Phone List.
		consumer.setCellPhoneNumber("123456789");
		expectedErrors.clear();
		expectedErrors.add("383");
		runInvalid(consumer, expectedErrors);
		
		// Test all the same number.
		consumer.setCellPhoneNumber("777777");
		expectedErrors.clear();
		expectedErrors.add("667");
		runInvalid(consumer, expectedErrors);
	}

	@Test
	public void testAddressLine1_Valid() throws Exception {
		List<ConsumerBO> consumers = new ArrayList<ConsumerBO>();
		ConsumerBO consumer = ObjectFactory.createMinimalValidConsumer();
		AddressBO address = ObjectFactory.createMinimalValidAddress();
		consumer.setAddress(address);
		consumers.add(consumer);
		
		List<RuleErrorBO> errorList = validator.validate(consumers);
		Assert.assertTrue("errorList should be empty", errorList.isEmpty());
	}
	
	@Test
	public void testAddressLine1_Invalid() throws Exception {
		Set<String> expectedErrors = new HashSet<String>();

		ConsumerBO consumer = ObjectFactory.createMinimalValidConsumer();
		AddressBO address = new AddressBO();
		consumer.setAddress(address);
		address.setAddressLine1("1515 Utica= Ave");
		expectedErrors.add("379");

		runInvalid(consumer, expectedErrors);
	}
	
	@Test
	public void testAddressLine2_Valid() throws Exception {
		List<ConsumerBO> consumers = new ArrayList<ConsumerBO>();
		ConsumerBO consumer = ObjectFactory.createMinimalValidConsumer();
		AddressBO address = ObjectFactory.createMinimalValidAddress();
		consumer.setAddress(address);
		consumers.add(consumer);
		
		List<RuleErrorBO> errorList = validator.validate(consumers);
		Assert.assertTrue("errorList should be empty", errorList.isEmpty());
	}
	
	@Test
	public void testAddressLine2_Invalid() throws Exception {
		Set<String> expectedErrors = new HashSet<String>();

		ConsumerBO consumer = ObjectFactory.createMinimalValidConsumer();
		AddressBO address = new AddressBO();
		consumer.setAddress(address);
		address.setAddressLine2("=100");
		expectedErrors.add("379");

		runInvalid(consumer, expectedErrors);
	}

	@Test
	public void testAddressLine3_Valid() throws Exception {
		List<ConsumerBO> consumers = new ArrayList<ConsumerBO>();
		ConsumerBO consumer = ObjectFactory.createMinimalValidConsumer();
		AddressBO address = ObjectFactory.createMinimalValidAddress();
		consumer.setAddress(address);
		consumers.add(consumer);
		
		List<RuleErrorBO> errorList = validator.validate(consumers);
		Assert.assertTrue("errorList should be empty", errorList.isEmpty());
	}
	
	@Test
	public void testAddressLine3_Invalid() throws Exception {
		Set<String> expectedErrors = new HashSet<String>();

		ConsumerBO consumer = ObjectFactory.createMinimalValidConsumer();
		AddressBO address = new AddressBO();
		consumer.setAddress(address);
		address.setAddressLine3("c\\o Dan");
		expectedErrors.add("379");

		runInvalid(consumer, expectedErrors);
	}

	@Test
	public void testAddress_InBadAddressList() throws Exception {
		Set<String> expectedErrors = new HashSet<String>();
		
		// InList operator.
		ConsumerBO consumer = ObjectFactory.createMinimalValidConsumer();
		AddressBO address = ObjectFactory.createBadAddressListAddress();
		consumer.setAddress(address);
		expectedErrors.add("382");
		
		runInvalid(consumer, expectedErrors);
		
		// InDynaFieldsList operator.
		address.setCity(null); // So it doesn't match the InList operator.
		expectedErrors.clear();
		expectedErrors.add("382");
		runInvalid(consumer, expectedErrors);
	}

	@Test
	public void testCity_Valid() throws Exception {
		List<ConsumerBO> consumers = new ArrayList<ConsumerBO>();
		ConsumerBO consumer = ObjectFactory.createMinimalValidConsumer();
		AddressBO address = ObjectFactory.createMinimalValidAddress();
		consumer.setAddress(address);
		consumers.add(consumer);
		
		List<RuleErrorBO> errorList = validator.validate(consumers);
		Assert.assertTrue("errorList should be empty", errorList.isEmpty());
	}

	@Test
	public void testCity_Invalid() throws Exception {
		Set<String> expectedErrors = new HashSet<String>();

		ConsumerBO consumer = ObjectFactory.createMinimalValidConsumer();
		AddressBO address = new AddressBO();
		consumer.setAddress(address);
		address.setCity("!@#$%^&*()-_=+");
		expectedErrors.add("379");

		runInvalid(consumer, expectedErrors);
	}
	
	@Test
	public void testState_Valid() throws Exception {
		List<ConsumerBO> consumers = new ArrayList<ConsumerBO>();
		ConsumerBO consumer = ObjectFactory.createMinimalValidConsumer();
		AddressBO address = ObjectFactory.createMinimalValidAddress();
		consumer.setAddress(address);
		consumers.add(consumer);
		
		List<RuleErrorBO> errorList = validator.validate(consumers);
		Assert.assertTrue("errorList should be empty", errorList.isEmpty());
	}

	@Test
	public void testState_Invalid() throws Exception {
		Set<String> expectedErrors = new HashSet<String>();

		ConsumerBO consumer = ObjectFactory.createMinimalValidConsumer();
		AddressBO address = new AddressBO();
		consumer.setAddress(address);
		address.setStateProvince("!@#$%^&*()-_=+");
		expectedErrors.add("379");

		runInvalid(consumer, expectedErrors);
	}

	@Test
	public void testIsoCountryCode_Valid() throws Exception {
		List<ConsumerBO> consumers = new ArrayList<ConsumerBO>();
		ConsumerBO consumer = ObjectFactory.createMinimalValidConsumer();
		AddressBO address = ObjectFactory.createMinimalValidAddress();
		consumer.setAddress(address);
		consumers.add(consumer);
		
		List<RuleErrorBO> errorList = validator.validate(consumers);
		Assert.assertTrue("errorList should be empty", errorList.isEmpty());
	}

	@Test
	public void testIsoCountryCode_Invalid() throws Exception {
		Set<String> expectedErrors = new HashSet<String>();

		ConsumerBO consumer = ObjectFactory.createMinimalValidConsumer();
		AddressBO address = new AddressBO();
		consumer.setAddress(address);
		address.setIsoCountryCode("!@#$%^&*()-_=+");
		expectedErrors.add("379");

		runInvalid(consumer, expectedErrors);
	}
	
	@Test
	public void testPersonalId1_Valid() throws Exception {
		List<ConsumerBO> consumers = new ArrayList<ConsumerBO>();
		ConsumerBO consumer = ObjectFactory.createMinimalValidConsumer();
		PersonalIDBO personalID = ObjectFactory.createMinimalValidPersonalID();
		consumer.setPersonalID1(personalID);
		consumers.add(consumer);
		
		List<RuleErrorBO> errorList = validator.validate(consumers);
		Assert.assertTrue("errorList should be empty", errorList.isEmpty());
		
		personalID.setPersonalIDType("1");
		personalID.setPersonalIDNumber("A1123456789Z");
		
		errorList = validator.validate(consumers);
		Assert.assertTrue("errorList should be empty", errorList.isEmpty());
		
		personalID.setPersonalIDType("S");
		personalID.setIsoCountryCode("USA");
		personalID.setPersonalIDNumber("951-37-2846");
		errorList = validator.validate(consumers);
		Assert.assertTrue("errorList should be empty", errorList.isEmpty());

	}

	@Test
	public void testPersonalID1_Invalid() throws Exception {
		Set<String> expectedErrors = new HashSet<String>();
		
		ConsumerBO consumer = ObjectFactory.createMinimalValidConsumer();
		PersonalIDBO personalID = ObjectFactory.createMinimalValidPersonalID();
		consumer.setPersonalID1(personalID);
		
		// Rule1: Invalid USA id number.
		personalID.setPersonalIDNumber("#T555");
		expectedErrors.add("376");

		runInvalid(consumer, expectedErrors);
		
		// Rule2: minimum length.
		expectedErrors.clear();
		personalID.setPersonalIDNumber("123");
		expectedErrors.add("377");
		
		runInvalid(consumer, expectedErrors);

		// Rule3: invalid value.
		expectedErrors.clear();
		expectedErrors.add("601");
		personalID.setPersonalIDType("INT");
		
		personalID.setPersonalIDNumber("12345");
		runInvalid(consumer, expectedErrors);
		
		personalID.setPersonalIDNumber("111111111");
		runInvalid(consumer, expectedErrors);
		
		// Rule4: drivers license sequence.
		expectedErrors.clear();
		expectedErrors.add("601");

		personalID.setPersonalIDType("1");
		personalID.setPersonalIDNumber("123456");
		runInvalid(consumer, expectedErrors);
		
		personalID.setPersonalIDType("DRV");
		personalID.setPersonalIDNumber("123456");
		runInvalid(consumer, expectedErrors);
		
		personalID.setPersonalIDNumber("654321");
		runInvalid(consumer, expectedErrors);
		
		personalID.setPersonalIDNumber("234567");
		runInvalid(consumer, expectedErrors);
		
		personalID.setPersonalIDNumber("765432");
		runInvalid(consumer, expectedErrors);
		
		personalID.setPersonalIDNumber("A234567");
		runInvalid(consumer, expectedErrors);
		
		personalID.setPersonalIDNumber("a234567");
		runInvalid(consumer, expectedErrors);
		
		personalID.setPersonalIDNumber("Z234567");
		runInvalid(consumer, expectedErrors);
		
		personalID.setPersonalIDNumber("z234567");
		runInvalid(consumer, expectedErrors);
		
		personalID.setPersonalIDNumber("999999999");
		runInvalid(consumer, expectedErrors);
		
		// Rule4: passport.
		expectedErrors.clear();
		expectedErrors.add("601");
		
		personalID.setPersonalIDType("2");
		personalID.setPersonalIDNumber("123456789");
		runInvalid(consumer, expectedErrors);
		
		personalID.setPersonalIDType("PAS");
		personalID.setPersonalIDNumber("123456789");
		runInvalid(consumer, expectedErrors);
		
		personalID.setPersonalIDNumber("987654321");
		runInvalid(consumer, expectedErrors);
		
		personalID.setPersonalIDNumber("111111111");
		runInvalid(consumer, expectedErrors);
		
		personalID.setPersonalIDNumber("999999999");
		runInvalid(consumer, expectedErrors);
		
		// Rule4: Legal ID sequence.
		expectedErrors.clear();
		expectedErrors.add("601");
		
		personalID.setPersonalIDType("I");
		personalID.setPersonalIDNumber("123456");
		runInvalid(consumer, expectedErrors);
		personalID.setPersonalIDNumber("987654");
		runInvalid(consumer, expectedErrors);
		personalID.setPersonalIDNumber("111111");
		runInvalid(consumer, expectedErrors);
		personalID.setPersonalIDNumber("999999");
		runInvalid(consumer, expectedErrors);

		personalID.setPersonalIDType("INT");
		personalID.setPersonalIDNumber("123456");
		runInvalid(consumer, expectedErrors);
		personalID.setPersonalIDNumber("987654");
		runInvalid(consumer, expectedErrors);
		personalID.setPersonalIDNumber("111111");
		runInvalid(consumer, expectedErrors);
		personalID.setPersonalIDNumber("999999");
		runInvalid(consumer, expectedErrors);

		personalID.setPersonalIDType("3");
		personalID.setPersonalIDNumber("123456");
		runInvalid(consumer, expectedErrors);
		personalID.setPersonalIDNumber("987654");
		runInvalid(consumer, expectedErrors);
		personalID.setPersonalIDNumber("111111");
		runInvalid(consumer, expectedErrors);
		personalID.setPersonalIDNumber("999999");
		runInvalid(consumer, expectedErrors);

		personalID.setPersonalIDType("STA");
		personalID.setPersonalIDNumber("123456");
		runInvalid(consumer, expectedErrors);
		personalID.setPersonalIDNumber("987654");
		runInvalid(consumer, expectedErrors);
		personalID.setPersonalIDNumber("111111");
		runInvalid(consumer, expectedErrors);
		personalID.setPersonalIDNumber("999999");
		runInvalid(consumer, expectedErrors);

		personalID.setPersonalIDType("T");
		personalID.setPersonalIDNumber("123456");
		runInvalid(consumer, expectedErrors);
		personalID.setPersonalIDNumber("987654");
		runInvalid(consumer, expectedErrors);
		personalID.setPersonalIDNumber("111111");
		runInvalid(consumer, expectedErrors);
		personalID.setPersonalIDNumber("999999");
		runInvalid(consumer, expectedErrors);

		personalID.setPersonalIDType("4");
		personalID.setPersonalIDNumber("123456");
		runInvalid(consumer, expectedErrors);
		personalID.setPersonalIDNumber("987654");
		runInvalid(consumer, expectedErrors);
		personalID.setPersonalIDNumber("111111");
		runInvalid(consumer, expectedErrors);
		personalID.setPersonalIDNumber("999999");
		runInvalid(consumer, expectedErrors);

		// Rule4: invalid SSN sequence.
		expectedErrors.clear();
		expectedErrors.add("601");
		
		personalID.setPersonalIDType("S");
		personalID.setIsoCountryCode("USA");
		personalID.setPersonalIDNumber("123456789");
		runInvalid(consumer, expectedErrors);
		
		personalID.setPersonalIDNumber("123-45-6789");
		runInvalid(consumer, expectedErrors);
		
		personalID.setPersonalIDNumber("987654321");
		runInvalid(consumer, expectedErrors);
		
		personalID.setPersonalIDNumber("987-65-4321");
		runInvalid(consumer, expectedErrors);
		
		personalID.setPersonalIDNumber("1236547890");
		runInvalid(consumer, expectedErrors);
		
		personalID.setPersonalIDNumber("12365478");
		runInvalid(consumer, expectedErrors);

		personalID.setPersonalIDNumber("1W3654789");
		runInvalid(consumer, expectedErrors);
		
		personalID.setPersonalIDNumber("12345a7891");
		runInvalid(consumer, expectedErrors);
	}
	

	@Test
	public void testPersonalId2_Valid() throws Exception {
		List<ConsumerBO> consumers = new ArrayList<ConsumerBO>();
		ConsumerBO consumer = ObjectFactory.createMinimalValidConsumer();
		PersonalIDBO personalID = ObjectFactory.createMinimalValidPersonalID();
		consumer.setPersonalID2(personalID);
		consumers.add(consumer);
		
		List<RuleErrorBO> errorList = validator.validate(consumers);
		Assert.assertTrue("errorList should be empty", errorList.isEmpty());
	}

	@Test
	public void testPersonalID2_Invalid() throws Exception {
		Set<String> expectedErrors = new HashSet<String>();
		
		ConsumerBO consumer = ObjectFactory.createMinimalValidConsumer();
		PersonalIDBO personalID = ObjectFactory.createMinimalValidPersonalID();
		consumer.setPersonalID2(personalID);
		
		// Rule1: Invalid USA id number.
		personalID.setPersonalIDNumber("#T555");
		expectedErrors.add("376");

		runInvalid(consumer, expectedErrors);
		
		// Rule2: minimum length.
		expectedErrors.clear();
		personalID.setPersonalIDNumber("123");
		expectedErrors.add("377");
		
		runInvalid(consumer, expectedErrors);

		// Rule3: invalid value.
		expectedErrors.clear();
		personalID.setPersonalIDType("INT");
		personalID.setPersonalIDNumber("12345");
		expectedErrors.add("601");
		
		runInvalid(consumer, expectedErrors);
	}

	@Test
	public void testPersonalId3_Valid() throws Exception {
		List<ConsumerBO> consumers = new ArrayList<ConsumerBO>();
		ConsumerBO consumer = ObjectFactory.createMinimalValidConsumer();
		PersonalIDBO personalID = ObjectFactory.createMinimalValidPersonalID();
		consumer.setPersonalID3(personalID);
		consumers.add(consumer);
		
		List<RuleErrorBO> errorList = validator.validate(consumers);
		Assert.assertTrue("errorList should be empty", errorList.isEmpty());
	}

	@Test
	public void testPersonalID3_Invalid() throws Exception {
		Set<String> expectedErrors = new HashSet<String>();
		
		ConsumerBO consumer = ObjectFactory.createMinimalValidConsumer();
		PersonalIDBO personalID = ObjectFactory.createMinimalValidPersonalID();
		consumer.setPersonalID3(personalID);
		
		// Rule1: Invalid USA id number.
		personalID.setPersonalIDNumber("#T555");
		expectedErrors.add("376");

		runInvalid(consumer, expectedErrors);
		
		// Rule2: minimum length.
		expectedErrors.clear();
		personalID.setPersonalIDNumber("123");
		expectedErrors.add("377");
		
		runInvalid(consumer, expectedErrors);

		// Rule3: invalid value.
		expectedErrors.clear();
		personalID.setPersonalIDType("INT");
		personalID.setPersonalIDNumber("12345");
		expectedErrors.add("601");
		
		runInvalid(consumer, expectedErrors);
	}

	@Test
	public void testPostalCode_Valid() throws Exception {
		List<ConsumerBO> consumers = new ArrayList<ConsumerBO>();
		ConsumerBO consumer = ObjectFactory.createMinimalValidConsumer();
		AddressBO address = ObjectFactory.createMinimalValidAddress();
		consumer.setAddress(address);
		consumers.add(consumer);
		
		List<RuleErrorBO> errorList = validator.validate(consumers);
		Assert.assertTrue("errorList should be empty", errorList.isEmpty());
	}

	@Test
	public void testPostalCode_Invalid() throws Exception {
		Set<String> expectedErrors = new HashSet<String>();

		ConsumerBO consumer = ObjectFactory.createMinimalValidConsumer();
		AddressBO address = new AddressBO();
		consumer.setAddress(address);
		
		// Rule1:
		address.setIsoCountryCode(null);
		address.setPostalCode("ZZ");
		expectedErrors.add("372");
		expectedErrors.add("373");

		runInvalid(consumer, expectedErrors);
		
		// Rule2:
		expectedErrors.clear();
		address.setIsoCountryCode("USA");
		address.setPostalCode("5551@");
		expectedErrors.add("373");

		runInvalid(consumer, expectedErrors);
		
		// Rule3:
		expectedErrors.clear();
		address.setIsoCountryCode("CAN");
		address.setPostalCode("55512");
		expectedErrors.add("374");

		runInvalid(consumer, expectedErrors);
		
	}
	
	@Test
	public void testBirthdate_Valid() throws Exception {
		List<ConsumerBO> consumers = new ArrayList<ConsumerBO>();
		ConsumerBO consumer = ObjectFactory.createMinimalValidConsumer();
		consumers.add(consumer);
		
		SimpleDateFormat sdf = new SimpleDateFormat("MM-DD-yyyy");
		consumer.setBirthDate(sdf.parse("01-01-1970"));
		
		List<RuleErrorBO> errorList = validator.validate(consumers);
		Assert.assertTrue("errorList should be empty", errorList.isEmpty());
	}

	@Test
	public void testBirthdate_InValid() throws Exception {
		Set<String> expectedErrors = new HashSet<String>();
		ConsumerBO consumer = ObjectFactory.createMinimalValidConsumer();
		
		SimpleDateFormat sdf = new SimpleDateFormat("MM-DD-yyyy");
		
		// Rule1: After. (370)
		// Rule2: After sys date. (371)
		consumer.setBirthDate(sdf.parse("01-01-2390"));
		expectedErrors.add("370");
		expectedErrors.add("371");
		
		runInvalid(consumer, expectedErrors);
		
		// Rule1: Before
		expectedErrors.clear();
		consumer.setBirthDate(sdf.parse("12-31-1899"));
		expectedErrors.add("370");
		
		runInvalid(consumer, expectedErrors);
		
		// Rule2:
		expectedErrors.clear();
		consumer.setBirthDate(Calendar.getInstance().getTime());
		expectedErrors.add("371");
		
		runInvalid(consumer, expectedErrors);
	}
	
	@Test
	public void testName_InBadConsumerNameList() throws Exception {
		Set<String> expectedErrors = new HashSet<String>();
		ConsumerBO consumer = new ConsumerBO();
		consumer.setFirstName("MONEYGRAM");
		consumer.setLastName("MONEYGRAM");
		expectedErrors.add("368");
		
		runInvalid(consumer, expectedErrors);
	}

	@Override
	public void setApplicationContext(ApplicationContext arg0)
			throws BeansException {
		this.appCtx = arg0;
	}
	
	private ApplicationContext appCtx;
	
	protected List<RuleErrorBO> runInvalid(ConsumerBO consumer, Set<String> expectedErrors) throws Exception {
		
		List<ConsumerBO> consumers = new ArrayList<ConsumerBO>();
		consumers.add(consumer);

		List<RuleErrorBO> errorList = validator.validate(consumers);
		Assert.assertFalse("errorList should not be empty", errorList.isEmpty());

		for (RuleErrorBO ruleError : errorList) {
			String errorCode = ruleError.getRuleError().getErrorCode();
			Assert.assertTrue(
					String.format("Unexpected error code: %s", errorCode),
					expectedErrors.contains(errorCode));
		}

		return errorList;
	}
	
	protected Map<String, ConsumerBO> generateMinimalInvalidConsumerMap() {
		Map<String, ConsumerBO> consumerMap = new HashMap<String, ConsumerBO>();
		
		ConsumerBO consumer;
		consumer = ObjectFactory.createMinimalInvalidConsumer();
		consumerMap.put(consumer.getValidationID(), consumer);
		
		consumer = ObjectFactory.createMinimalInvalidConsumer();
		consumerMap.put(consumer.getValidationID(), consumer);

		consumer = ObjectFactory.createMinimalInvalidConsumer();
		consumerMap.put(consumer.getValidationID(), consumer);
		
		return consumerMap;
	}
	
	protected Map<String, ConsumerBO> generateMinimalValidConsumerMap() {
		Map<String, ConsumerBO> consumerMap = new HashMap<String, ConsumerBO>();
		
		ConsumerBO consumer;
		consumer = ObjectFactory.createMinimalValidConsumer();
		consumerMap.put(consumer.getValidationID(), consumer);
		
		consumer = ObjectFactory.createMinimalValidConsumer();
		consumerMap.put(consumer.getValidationID(), consumer);

		consumer = ObjectFactory.createMinimalValidConsumer();
		consumerMap.put(consumer.getValidationID(), consumer);
		
		return consumerMap;
	}
}
