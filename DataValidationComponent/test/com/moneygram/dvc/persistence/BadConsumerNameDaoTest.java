package com.moneygram.dvc.persistence;

import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.moneygram.dvc.bo.ConsumerNameBO;
import com.moneygram.dvc.persistence.BadConsumerNameDao.BadConsumerNameMapper;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations="classpath*:/META-INF/spring/dvc-test-component.xml")
public class BadConsumerNameDaoTest {

	@Autowired
	BadConsumerNameDao dao;
	
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testGetRowMapper() {
		RowMapper<?> mapper = dao.getRowMapper();
		Assert.assertNotNull("getRowMapper returned null.", mapper);
		Assert.assertTrue("Invalid RowMapper instance", mapper instanceof BadConsumerNameMapper);
	}
	
	@Test
	public void testGetStoredProcedureName() {
		String s = dao.getStoredProcedureName();
		Assert.assertNotNull("getStoredProcedureName returned null.", s);
		Assert.assertEquals(BadConsumerNameDao.BAD_CONSUMER_NAME_PROC_NAME, s);
	}
	
	@Test 
	public void testGetProcessLogIdOutputName() {
		String s = dao.getProcessLogIdOutputName();
		Assert.assertNotNull("getProcessLogIdOutputName returned null.", s);
		Assert.assertEquals(BadConsumerNameDao.PROCESS_LOG_ID, s);
	}

	@Test
	public void testGetBusinessAreaCodeInputName() {
		String s = dao.getBusinessAreaCodeInputName();
		Assert.assertNotNull("getBusinessAreaCodeInputName returned null.", s);
		Assert.assertEquals(BadConsumerNameDao.BUSINESS_CODE, s);
	}

	@Test
	public void testGetCallTypeCodeInputName() {
		String s = dao.getCallTypeCodeInputName();
		Assert.assertNotNull("getCallTypeCodeInputName returned null.", s);
		Assert.assertEquals(BadConsumerNameDao.CALL_TYPE_CODE, s);
	}

	@Test
	public void testGetCursorOutputName() {
		String s = dao.getCursorOutputName();
		Assert.assertNotNull("getCursorOutputName returned null.", s);
		Assert.assertEquals(BadConsumerNameDao.OUTPUT_CURSOR, s);
	}

	@Test
	public void testFind() throws Exception {
		List<?> names = dao.find();
		
		Assert.assertNotNull("find returned null.", names);
		Assert.assertFalse("expecting a non empty list", names.isEmpty());
		Assert.assertTrue("returned incorrect type", names.get(0) instanceof ConsumerNameBO);
	}

	@Test
	public void testFindInvalidBusinessAreaCode() throws Exception {
		// Change business area code to some non-existent code.
		dao.setBusinessAreaCode("ZZZXYZ");
		
		List<?> names = dao.find();

		Assert.assertNotNull("find returned null.", names);
		Assert.assertTrue("expecting an empty list", names.isEmpty());

	}
}
