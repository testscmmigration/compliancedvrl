package com.moneygram.dvr.service.command;

import java.util.Calendar;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.moneygram.common_v1.AgentHeader;
import com.moneygram.common_v1.BaseServiceRequestMessage;
import com.moneygram.common_v1.ClientHeader;
import com.moneygram.common_v1.Header;
import com.moneygram.common_v1.ProcessingInstruction;
import com.moneygram.dvc.test.TestRCSFacade;
import com.moneygram.rule.common.cache.CachedRuleSetFactory;
import com.moneygram.service.dvr_v1.ArrayOfConsumer;
import com.moneygram.service.dvr_v1.Consumer;
import com.moneygram.service.dvr_v1.ConsumerValidationRequest;
import com.moneygram.service.dvr_v1.ConsumerValidationReturn;
import com.moneygram.service.dvr_v1.ErrorDetail;
import com.moneygram.service.dvr_v1.ObjectFactory;
import com.moneygram.service.dvr_v1.PersonalID;
import com.moneygram.service.framework.command.CommandException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations={"/META-INF/spring/DVRapplicationContext-test.xml", "/META-INF/spring/dvc-test-component.xml"}) 
public class ValidateConsumersCommandTest implements ApplicationContextAware {

	@Autowired
	private ValidateConsumersCommand cmd;
	private ObjectFactory objectFactory;
	
	@Before
	public void setUp() throws Exception {
		objectFactory = new ObjectFactory();
		
		CachedRuleSetFactory crsf =	(CachedRuleSetFactory) appCtx.getBean("mgirulecommon.cachedRuleSetFactory");
		crsf.setRuleConfigurationFacade(new TestRCSFacade());
	}

	@Test
	public void testGetResponseToReturnError() {
		Object o = cmd.getResponseToReturnError();
		
		Assert.assertNotNull("getResponseToReturnError() returned null.", o);
		Assert.assertTrue("Expecting instance of ConsumerValidationReturn", o instanceof ConsumerValidationReturn);
	}

	@Test
	public void testIsRequestSupportedBaseServiceRequestMessage() throws CommandException {
		BaseServiceRequestMessage bsrm = objectFactory.createConsumerValidationRequest();
		Assert.assertTrue("Expecting true.", cmd.isRequestSupported(bsrm));
		
		Assert.assertFalse("Expecting false.", cmd.isRequestSupported(new BaseServiceRequestMessage()));
		
		Assert.assertFalse(cmd.isRequestSupported(null));
	}

	@Test
	public void testProcess_OneConsumerValid() throws CommandException {
		ConsumerValidationRequest request = createValidateConsumerRequest();
		ArrayOfConsumer consumers = objectFactory.createArrayOfConsumer();
		request.setConsumers(consumers);
		consumers.getItems().add(createValidConsumer(UUID.randomUUID().toString()));
		
		ConsumerValidationReturn result = (ConsumerValidationReturn) cmd.process(request);
		
		Assert.assertNotNull("result is null.", result);
		Assert.assertEquals(request.getHeader(), result.getHeader());
		Assert.assertNull("validationErrors should be null", result.getValidationErrors());
	}

	@Test
	public void testProcess_OneConsumerInvalid() throws CommandException {
		ConsumerValidationRequest request = createValidateConsumerRequest();
		ArrayOfConsumer consumers = objectFactory.createArrayOfConsumer();
		request.setConsumers(consumers);
		
		String validationId = UUID.randomUUID().toString();
		consumers.getItems().add(createInValidConsumer(validationId));
		
		ConsumerValidationReturn result = (ConsumerValidationReturn) cmd.process(request);
		Assert.assertNotNull("result is null.", result);
		Assert.assertEquals(request.getHeader(), result.getHeader());
		Assert.assertNotNull("validationErrors is null.", result.getValidationErrors());
		Assert.assertFalse("validationErrors is empty", result.getValidationErrors().getItems().isEmpty());
		Assert.assertEquals("Expected one.", 1, result.getValidationErrors().getItems().size());
		
		ErrorDetail errorDetail = result.getValidationErrors().getItems().get(0);
		Assert.assertEquals("validationID:", String.valueOf(validationId), errorDetail.getValidationID());
		
	}
	
	@Test
	public void testProcess_MultipleConsumersValid() throws CommandException {
		ConsumerValidationRequest request = createValidateConsumerRequest();
		ArrayOfConsumer consumers = objectFactory.createArrayOfConsumer();
		request.setConsumers(consumers);
		consumers.getItems().add(createValidConsumer(UUID.randomUUID().toString()));
		consumers.getItems().add(createValidConsumer(UUID.randomUUID().toString()));
		consumers.getItems().add(createValidConsumer(UUID.randomUUID().toString()));
		
		ConsumerValidationReturn result = (ConsumerValidationReturn) cmd.process(request);
		
		Assert.assertNotNull("result is null.", result);
		Assert.assertEquals(request.getHeader(), result.getHeader());
		Assert.assertNull("validationErrors should be null", result.getValidationErrors());
	}
	
	@Test
	public void testProcess_MultipleConsumersInValid() throws CommandException {
		ConsumerValidationRequest request = createValidateConsumerRequest();
		ArrayOfConsumer consumers = objectFactory.createArrayOfConsumer();
		request.setConsumers(consumers);
		
		Set<String> validationIDs = new HashSet<String>();
		String validationId = UUID.randomUUID().toString();
		validationIDs.add(validationId);
		consumers.getItems().add(createInValidConsumer(validationId));
		
		validationId = UUID.randomUUID().toString();
		validationIDs.add(validationId);
		consumers.getItems().add(createInValidConsumer(validationId));

		validationId = UUID.randomUUID().toString();
		validationIDs.add(validationId);
		consumers.getItems().add(createInValidConsumer(validationId));
		
		ConsumerValidationReturn result = (ConsumerValidationReturn) cmd.process(request);
		
		Assert.assertNotNull("result is null.", result);
		Assert.assertEquals(request.getHeader(), result.getHeader());
		Assert.assertNotNull("validationErrors is null.", result.getValidationErrors());
		Assert.assertFalse("validationErrors is empty", result.getValidationErrors().getItems().isEmpty());
		Assert.assertEquals("Expected number of errors:", consumers.getItems().size(), result.getValidationErrors().getItems().size());
		
		for (ErrorDetail errorDetail : result.getValidationErrors().getItems()) {
			Assert.assertTrue("validationID:", validationIDs.contains(errorDetail.getValidationID()));
		}
	}
	
	@Test
	public void testProcess_MixOfValidAndInvalid() throws CommandException {
		ConsumerValidationRequest request = createValidateConsumerRequest();
		ArrayOfConsumer consumers = objectFactory.createArrayOfConsumer();
		request.setConsumers(consumers);

		Set<String> validationIDs = new HashSet<String>();
		String validationId = UUID.randomUUID().toString();
		validationIDs.add(validationId);
		
		consumers.getItems().add(createValidConsumer(UUID.randomUUID().toString()));
		consumers.getItems().add(createInValidConsumer(validationId));
		consumers.getItems().add(createValidConsumer(UUID.randomUUID().toString()));
		validationId = UUID.randomUUID().toString();
		validationIDs.add(validationId);
		consumers.getItems().add(createInValidConsumer(validationId));
		consumers.getItems().add(createValidConsumer(UUID.randomUUID().toString()));

		ConsumerValidationReturn result = (ConsumerValidationReturn) cmd.process(request);

		Assert.assertNotNull("validationErrors is null.", result.getValidationErrors());
		Assert.assertFalse("validationErrors is empty", result.getValidationErrors().getItems().isEmpty());
		Assert.assertEquals("Expected number of errors:", validationIDs.size(), result.getValidationErrors().getItems().size());

		for (ErrorDetail errorDetail : result.getValidationErrors().getItems()) {
			Assert.assertTrue("validationID:", validationIDs.contains(errorDetail.getValidationID()));
		}
	}
	
	@Test
	public void testProcess_NullBirthDateField() throws CommandException {
		ConsumerValidationRequest request = createValidateConsumerRequest();
		ArrayOfConsumer consumers = objectFactory.createArrayOfConsumer();
		request.setConsumers(consumers);
		
		Consumer consumer = createValidConsumer(UUID.randomUUID().toString());
		consumer.setBirthDate(null);
		
		consumers.getItems().add(consumer);
		
		ConsumerValidationReturn result = (ConsumerValidationReturn) cmd.process(request);
		
		Assert.assertNotNull("result is null.", result);
		Assert.assertEquals(request.getHeader(), result.getHeader());
		Assert.assertNull("validationErrors should be null", result.getValidationErrors());

	}

	protected ConsumerValidationRequest createValidateConsumerRequest() {
		ConsumerValidationRequest request = objectFactory.createConsumerValidationRequest();
		
		com.moneygram.common_v1.ObjectFactory commonOF = new com.moneygram.common_v1.ObjectFactory();
		
		Header header = commonOF.createHeader();
		request.setHeader(header);
		
		AgentHeader agentHeader = commonOF.createAgentHeader();
		header.setAgent(agentHeader);
		agentHeader.setAgentId("agt_123");
		agentHeader.setMainOfficeId("mo_id");
		agentHeader.setOperatorId("op_id");
		agentHeader.setTerminalId("t_id");
		
		ClientHeader clientHeader = commonOF.createClientHeader();
		header.setClientHeader(clientHeader);
		clientHeader.setClientName("client_name");
		clientHeader.setClientRequestID("rqst_id");
		clientHeader.setClientSessionID("s_id");
		
		ProcessingInstruction pi = commonOF.createProcessingInstruction();
		header.setProcessingInstruction(pi);
		pi.setAction("validateConsumers");
		pi.setEchoRequestFlag(true);
		pi.setReadOnlyFlag(true);

		header.setRoutingContext(commonOF.createRoutingContextHeader());
		header.setSecurity(commonOF.createSecurityHeader());
		
		return request;
	}

	private Consumer createValidConsumer(String validationID) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.YEAR, -21);
		
		Consumer consumer = objectFactory.createConsumer();
		consumer.setAddressLine1("123 Main");
		consumer.setBirthDate(cal);
		consumer.setCity("Minneapolis");
		consumer.setFirstName("John");
		consumer.setHomePhoneNumber("9525913120");
		consumer.setIsoCountryCode("USA");
		consumer.setLastName("Olson");
		consumer.setPersonalID1(createValidPersonalID());
		consumer.setPostalCode("55401");
		consumer.setStateProvince("MN");
		consumer.setValidationID(validationID);

		return consumer;
	}

	private PersonalID createValidPersonalID() {
		PersonalID id = objectFactory.createPersonalID();
		
		id.setIsoCountryCode("USA");
		id.setPersonalIDNumber("T129812221121");
		id.setPersonalIDType("DL");
		id.setStateProvince("MN");
		
		return id;
	}
	
	private Consumer createInValidConsumer(String validationID) {
		Consumer consumer = createValidConsumer(validationID);
		
		consumer.setFirstName("WAL,MART");
		consumer.setLastName("WAL,MART");
		return consumer;
	}

	private ApplicationContext appCtx;
	
	/* (non-Javadoc)
	 * @see org.springframework.context.ApplicationContextAware#setApplicationContext(org.springframework.context.ApplicationContext)
	 */
	@Override
	public void setApplicationContext(ApplicationContext arg0)
			throws BeansException {
		appCtx = arg0;
	}

}
