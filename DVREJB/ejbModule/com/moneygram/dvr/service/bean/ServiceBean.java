package com.moneygram.dvr.service.bean;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.xml.transform.Source;
import javax.xml.ws.Provider;
import javax.xml.ws.Service.Mode;
import javax.xml.ws.ServiceMode;
import javax.xml.ws.WebServiceProvider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.moneygram.common.util.ApplicationContextFactory;
import com.moneygram.common.util.ApplicationContextProvider;
import com.moneygram.service.framework.BaseService;
import com.moneygram.service.framework.bo.EJBResponse;
import com.moneygram.service.framework.router.RequestRouter;

/**
 * Session Bean implementation class ServiceBean
 */
@Stateless
@ServiceMode(value = Mode.PAYLOAD)
@WebServiceProvider(targetNamespace = "http://moneygram.com/service/DVR_v1", serviceName = "DVR", portName = "DVRPort", wsdlLocation = "META-INF/wsdl/DVR_v1.wsdl" )
public class ServiceBean extends BaseService implements ServiceBeanLocal, Provider<Source> {

	@Resource
	SessionContext ctx;
	
	private static final Logger logger = LoggerFactory.getLogger(ServiceBean.class);
	
    /**
     * @see BaseService#BaseService()
     */
    public ServiceBean() {
        super();
    }

	@Override
	public Source invoke(Source request) {
		return super.invoke(request);
	}

	@Override
	protected String processRequest(String s) {
		logger.debug(String.format("request = %s", s));
		
		ApplicationContextFactory.getApplicationContext();
		RequestRouter router = (RequestRouter) ApplicationContextProvider.getRootBean();
		
		EJBResponse response = router.process(s);
		if (response.isRollback()) {
			logger.error(String.format("Rollback requested for request:\n %s \n error:\n %s", s, response.getResponsePayload()));
			ctx.setRollbackOnly();
		}
		
		if (response.isReturnResponseAsException()) {
			return getSoapFault(response.getResponsePayload());
		}
		
		return response.getResponsePayload();
	}

}
