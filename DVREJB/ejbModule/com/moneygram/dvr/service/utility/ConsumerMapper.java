package com.moneygram.dvr.service.utility;

import com.moneygram.dvc.bo.AddressBO;
import com.moneygram.dvc.bo.ConsumerBO;
import com.moneygram.dvc.bo.PersonalIDBO;
import com.moneygram.service.dvr_v1.Consumer;
import com.moneygram.service.dvr_v1.PersonalID;

public class ConsumerMapper {

	public static ConsumerBO createConsumerBO(Consumer source) {
		ConsumerBO dest = new ConsumerBO();
		
		dest.setAddress(createAddressBO(source));
		dest.setBirthDate(source.getBirthDate() == null ? null : source.getBirthDate().getTime());
		dest.setCellPhoneNumber(source.getCellPhoneNumber());
		dest.setFirstName(source.getFirstName());
		dest.setHomePhoneNumber(source.getHomePhoneNumber());
		dest.setLastName(source.getLastName());
		dest.setMiddleName(source.getMiddleName());
		dest.setPersonalID1(createPersonalIDBO(source.getPersonalID1()));
		dest.setPersonalID2(createPersonalIDBO(source.getPersonalID2()));
		dest.setPersonalID3(createPersonalIDBO(source.getPersonalID3()));
		dest.setSecondLastName(source.getSecondLastName());
		dest.setValidationID(source.getValidationID());
		
		return dest;
	}
	
	private static AddressBO createAddressBO(Consumer source) {
		AddressBO dest = new AddressBO();
		
		dest.setAddressLine1(source.getAddressLine1());
		dest.setAddressLine2(source.getAddressLine2());
		dest.setAddressLine3(source.getAddressLine3());
		dest.setCity(source.getCity());
		dest.setIsoCountryCode(source.getIsoCountryCode());
		dest.setPostalCode(source.getPostalCode());
		dest.setStateProvince(source.getStateProvince());
		
		return dest;
	}

	private static PersonalIDBO createPersonalIDBO(PersonalID source) {
		if (source == null) {
			return null;
		}
		
		PersonalIDBO dest = new PersonalIDBO();
		
//		dest.setCategory("??");
		dest.setIsoCountryCode(source.getIsoCountryCode());
		dest.setPersonalIDNumber(source.getPersonalIDNumber());
		dest.setPersonalIDType(source.getPersonalIDType());
		dest.setStateProvince(source.getStateProvince());
		
		return dest;
	}
}
