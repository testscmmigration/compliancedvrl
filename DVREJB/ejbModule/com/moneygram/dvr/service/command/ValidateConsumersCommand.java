package com.moneygram.dvr.service.command;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.util.Assert;

import com.moneygram.common_v1.BaseServiceRequestMessage;
import com.moneygram.common_v1.BaseServiceResponseMessage;
import com.moneygram.common_v1.Error;
import com.moneygram.common_v1.ErrorCategoryCode;
import com.moneygram.common_v1.ErrorHandlingCode;
import com.moneygram.common_v1.Errors;
import com.moneygram.dvc.bo.ConsumerBO;
import com.moneygram.dvc.bo.RuleErrorBO;
import com.moneygram.dvc.consumervalidation.ConsumerValidationRuleManager;
import com.moneygram.dvr.service.utility.ConsumerMapper;
import com.moneygram.rule.common.bo.RuleError;
import com.moneygram.service.dvr_v1.ArrayOfErrorDetail;
import com.moneygram.service.dvr_v1.Consumer;
import com.moneygram.service.dvr_v1.ConsumerValidationRequest;
import com.moneygram.service.dvr_v1.ConsumerValidationReturn;
import com.moneygram.service.dvr_v1.ErrorDetail;
import com.moneygram.service.dvr_v1.ObjectFactory;
import com.moneygram.service.framework.command.CommandException;
import com.moneygram.service.framework.command.ReadCommand;
import com.moneygram.service.framework.command.SystemException;

public class ValidateConsumersCommand extends ReadCommand implements InitializingBean {

	private static final String FAILED_TO_VALIDATE_CONSUMERS = "Failed to Validate Consumers.";
	private static final Logger log = LoggerFactory.getLogger(ValidateConsumersCommand.class);
	
	@Override
	public BaseServiceResponseMessage getResponseToReturnError() {
		return objectFactory.createConsumerValidationReturn();
	}

	@Override
	protected boolean isRequestSupported(BaseServiceRequestMessage request)
			throws CommandException {
		return request instanceof ConsumerValidationRequest;
	}

	@Override
	protected BaseServiceResponseMessage process(BaseServiceRequestMessage request)
			throws CommandException {
		
		ConsumerValidationReturn response =	objectFactory.createConsumerValidationReturn();
		response.setHeader(request.getHeader());

		List<ConsumerBO> consumers = extractConsumerBOs(request);
		
		try {
			List<RuleErrorBO> errors = consumerValidationRuleManager.validate(consumers);
			
			if (errors != null && !errors.isEmpty()) {
				response.setValidationErrors(createArrayOfErrorDetail(errors));
			}
			
		} catch (Exception e) {
			log.error(FAILED_TO_VALIDATE_CONSUMERS, e);
			throw new SystemException(FAILED_TO_VALIDATE_CONSUMERS, e);
		}
		
		return response;
	}

	protected List<ConsumerBO> extractConsumerBOs(BaseServiceRequestMessage request) {
		
		ConsumerValidationRequest cvRequest = (ConsumerValidationRequest)request;
		List<ConsumerBO> consumers = new ArrayList<ConsumerBO>();
		
		for (Consumer consumer : cvRequest.getConsumers().getItems()) {
			consumers.add(ConsumerMapper.createConsumerBO(consumer));
		}
		
		return consumers;
	}

	protected ArrayOfErrorDetail createArrayOfErrorDetail(List<RuleErrorBO> errors) {
		Map<String, ErrorDetail> errorMap = new HashMap<String, ErrorDetail>();
		
		
		for (RuleErrorBO ruleErrorBO : errors) {
			ErrorDetail errorDetail;
			String validationID = ruleErrorBO.getValidationID();

			if (!errorMap.containsKey(validationID)) {
				errorDetail = new ErrorDetail();
				errorDetail.setErrors(new Errors());
				errorDetail.setValidationID(validationID);
				errorMap.put(validationID, errorDetail);
			}
			
			errorDetail = errorMap.get(validationID) ;
			errorDetail.getErrors().getErrors().add(createError(ruleErrorBO.getRuleError()));
		}
		
		ArrayOfErrorDetail arrayOfErrorDetail = new ArrayOfErrorDetail();
		arrayOfErrorDetail.getItems().addAll(errorMap.values());
		
		return arrayOfErrorDetail;
	}
	
	protected Error createError(RuleError source) {
		Error dest = new Error();
		
		dest.setErrorCategoryCode(ErrorCategoryCode.USER_ERROR);
		dest.setErrorCode(source.getErrorCode());
		dest.setErrorHandlingCode(ErrorHandlingCode.RETURN_ERROR);
		dest.setErrorLocation("validateConsumers");
		dest.setErrorMessage(source.getErrorMessage());
		dest.setErrorSource("DVS");
		dest.setOffendingField(source.getOffendingField());
		
		return dest;
	}


	/* (non-Javadoc)
	 * @see org.springframework.beans.factory.InitializingBean#afterPropertiesSet()
	 */
	@Override
	public void afterPropertiesSet() throws Exception {
		Assert.notNull(consumerValidationRuleManager, "consumerValidationRuleManager is null.");
		Assert.notNull(objectFactory, "objectFactory is null.");
	}

	public void setConsumerValidationRuleManager(
			ConsumerValidationRuleManager consumerValidationRuleManager) {
		this.consumerValidationRuleManager = consumerValidationRuleManager;
	}
	
	public void setObjectFactory(ObjectFactory objectFactory) {
		this.objectFactory = objectFactory;
	}
	
	// Spring DI properties.
	private ConsumerValidationRuleManager consumerValidationRuleManager;
	private ObjectFactory objectFactory;
}
