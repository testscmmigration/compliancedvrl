
package com.moneygram.service.dvr_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import com.moneygram.common_v1.BaseServiceRequestMessage;


/**
 * <p>Java class for ConsumerValidationRequest complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ConsumerValidationRequest">
 *   &lt;complexContent>
 *     &lt;extension base="{http://moneygram.com/common_v1}BaseServiceRequestMessage">
 *       &lt;sequence>
 *         &lt;element name="consumers" type="{http://moneygram.com/service/DVR_v1}ArrayOfConsumer"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ConsumerValidationRequest", propOrder = {
    "consumers"
})
@XmlRootElement(name = "consumerValidationRequest")
public class ConsumerValidationRequest
    extends BaseServiceRequestMessage
{

    @XmlElement(required = true, nillable = true)
    protected ArrayOfConsumer consumers;

    /**
     * Gets the value of the consumers property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfConsumer }
     *     
     */
    public ArrayOfConsumer getConsumers() {
        return consumers;
    }

    /**
     * Sets the value of the consumers property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfConsumer }
     *     
     */
    public void setConsumers(ArrayOfConsumer value) {
        this.consumers = value;
    }

}
