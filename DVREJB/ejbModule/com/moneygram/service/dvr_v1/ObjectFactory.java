
package com.moneygram.service.dvr_v1;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.moneygram.service.dvr_v1 package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Fault_QNAME = new QName("http://moneygram.com/service/DVR_v1", "fault");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.moneygram.service.dvr_v1
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ConsumerValidationRequest }
     * 
     */
    public ConsumerValidationRequest createConsumerValidationRequest() {
        return new ConsumerValidationRequest();
    }

    /**
     * Create an instance of {@link ArrayOfConsumer }
     * 
     */
    public ArrayOfConsumer createArrayOfConsumer() {
        return new ArrayOfConsumer();
    }

    /**
     * Create an instance of {@link ConsumerValidationReturn }
     * 
     */
    public ConsumerValidationReturn createConsumerValidationReturn() {
        return new ConsumerValidationReturn();
    }

    /**
     * Create an instance of {@link ArrayOfErrorDetail }
     * 
     */
    public ArrayOfErrorDetail createArrayOfErrorDetail() {
        return new ArrayOfErrorDetail();
    }

    /**
     * Create an instance of {@link Consumer }
     * 
     */
    public Consumer createConsumer() {
        return new Consumer();
    }

    /**
     * Create an instance of {@link ErrorDetail }
     * 
     */
    public ErrorDetail createErrorDetail() {
        return new ErrorDetail();
    }

    /**
     * Create an instance of {@link PersonalID }
     * 
     */
    public PersonalID createPersonalID() {
        return new PersonalID();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://moneygram.com/service/DVR_v1", name = "fault")
    public JAXBElement<String> createFault(String value) {
        return new JAXBElement<String>(_Fault_QNAME, String.class, null, value);
    }

}
