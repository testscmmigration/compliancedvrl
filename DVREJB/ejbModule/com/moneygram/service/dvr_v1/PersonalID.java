
package com.moneygram.service.dvr_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PersonalID complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="PersonalID">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="personalIDType" type="{http://moneygram.com/service/DVR_v1}PersonalIDType"/>
 *         &lt;element name="personalIDNumber" type="{http://moneygram.com/service/DVR_v1}PersonalIDNumber"/>
 *         &lt;element name="isoCountryCode" type="{http://moneygram.com/service/DVR_v1}ISOCountryCode"/>
 *         &lt;element name="stateProvince" type="{http://moneygram.com/service/DVR_v1}StateProvince"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "PersonalID", propOrder = {
    "personalIDType",
    "personalIDNumber",
    "isoCountryCode",
    "stateProvince"
})
public class PersonalID {

    @XmlElement(required = true, nillable = true)
    protected String personalIDType;
    @XmlElement(required = true, nillable = true)
    protected String personalIDNumber;
    @XmlElement(required = true, nillable = true)
    protected String isoCountryCode;
    @XmlElement(required = true, nillable = true)
    protected String stateProvince;

    /**
     * Gets the value of the personalIDType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonalIDType() {
        return personalIDType;
    }

    /**
     * Sets the value of the personalIDType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonalIDType(String value) {
        this.personalIDType = value;
    }

    /**
     * Gets the value of the personalIDNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonalIDNumber() {
        return personalIDNumber;
    }

    /**
     * Sets the value of the personalIDNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonalIDNumber(String value) {
        this.personalIDNumber = value;
    }

    /**
     * Gets the value of the isoCountryCode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getIsoCountryCode() {
        return isoCountryCode;
    }

    /**
     * Sets the value of the isoCountryCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setIsoCountryCode(String value) {
        this.isoCountryCode = value;
    }

    /**
     * Gets the value of the stateProvince property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStateProvince() {
        return stateProvince;
    }

    /**
     * Sets the value of the stateProvince property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStateProvince(String value) {
        this.stateProvince = value;
    }

}
