
package com.moneygram.service.dvr_v1;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;
import com.moneygram.common_v1.Errors;


/**
 * <p>Java class for ErrorDetail complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ErrorDetail">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="validationID" type="{http://moneygram.com/service/DVR_v1}ValidationID"/>
 *         &lt;element name="errors" type="{http://moneygram.com/common_v1}Errors"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ErrorDetail", propOrder = {
    "validationID",
    "errors"
})
public class ErrorDetail {

    @XmlElement(required = true, nillable = true)
    protected String validationID;
    @XmlElement(required = true)
    protected Errors errors;

    /**
     * Gets the value of the validationID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValidationID() {
        return validationID;
    }

    /**
     * Sets the value of the validationID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValidationID(String value) {
        this.validationID = value;
    }

    /**
     * Gets the value of the errors property.
     * 
     * @return
     *     possible object is
     *     {@link Errors }
     *     
     */
    public Errors getErrors() {
        return errors;
    }

    /**
     * Sets the value of the errors property.
     * 
     * @param value
     *     allowed object is
     *     {@link Errors }
     *     
     */
    public void setErrors(Errors value) {
        this.errors = value;
    }

}
